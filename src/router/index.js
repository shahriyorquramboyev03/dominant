import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home/index.vue'
import Price from '../views/Price.vue'
import Auto from '../views/AutoMobile.vue'
import News from '../views/News.vue'
import Contact from '../views/Contact.vue'
import Jobs from '../views/Jobs.vue'
import Post from '../views/Post.vue'
import Newspost from '../views/NewsPost.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/price',
    name: 'Price',
    component: Price
  },
  {
    path: '/auto',
    name: 'Auto',
    component: Auto
  },
  {
    path: '/news',
    name: 'News',
    component: News
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/jobs',
    name: 'Jobs',
    component: Jobs
  },
  {
    path: '/post/:id',
    name: 'Post',
    component: Post
  },
  {
    path: '/post/:id',
    name: 'Newspost',
    component: Newspost
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
