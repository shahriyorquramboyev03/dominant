export default[
  {
    id:'b1',
    img:require('../src/images/product/image 71.png'),
    title:'Газель тент «Стандарт» ',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b2',
    img:require('../src/images/product/img-2.png'),
    title:'Газель тент «Удлиненная» ',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b3',
    img:require('../src/images/product/img-3.png'),
    title:'Газель тент «Удлиненная» ',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b4',
    img:require('../src/images/product/img-4.png'),
    title:'Газель бортовая 4-6м',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b5',
    img:require('../src/images/product/img-5.png'),
    title:'Газель с “пирамидой”',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b6',
    img:require('../src/images/product/img-6.png'),
    title:'ГАЗон будка ',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b7',
    img:require('../src/images/product/img-7.png'),
    title:'Самосвал Камаз 10м3',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b8',
    img:require('../src/images/product/img-8.png'),
    title:'Самосвал Камаз 16м3',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b9',
    img:require('../src/images/product/img-9.png'),
    title:'Самосвал MAN 20м3',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b10',
    img:require('../src/images/product/img-10.png'),
    title:'Газель рефрижератор 4м',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b11',
    img:require('../src/images/product/img-11.png'),
    title:'Погрузчик jcb ковш 1.2 м3',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b12',
    img:require('../src/images/product/img-12.png'),
    title:'Минипогрузчик/бобкэт jcb ковш 0.3м3',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b13',
    img:require('../src/images/product/img-13.png'),
    title:`Газель фермер`,
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b14',
    img:require('../src/images/product/img-14.png'),
    title:'“Каблук”',
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  {
    id:'b15',
    img:require('../src/images/product/img-15.png'),
    title:`MAN фургон с гидробортом`,
    price:'от 800 р/час',
    subtitle:`Грузоподъемность 2 тонны Длина 3 м Высота 2,5 м`
  },
  
]